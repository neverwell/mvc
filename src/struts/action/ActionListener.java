package struts.action;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import struts.form.StrutsXML;
import struts.form.XmlBean;

public class ActionListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("信息已经注销");
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		ServletContext context = arg0.getServletContext();
		String xmlPath = context.getInitParameter("struts-config");
		String tomcatPath = context.getRealPath("\\");
		try {
			Map<String, XmlBean> map = StrutsXML.ddd(tomcatPath + xmlPath);
			context.setAttribute("struts", map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("信息加载完成");
	}
}
