package struts;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import struts.action.Action;
import struts.form.ActionForm;
import struts.form.FullForm;
import struts.form.XmlBean;

/**
 * Servlet implementation class ActionServlet
 */
@WebServlet("*.do")
public class ActionServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7320431028733786256L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setCharacterEncoding("UTF-8");
		// Action action=new PandAction();
		// String url=action.execute(req);
		// RequestDispatcher dis = req.getRequestDispatcher(url);
		// dis.forward(req, resp);
		String path = this.getPath(req.getServletPath());
		Map<String, XmlBean> map = (Map<String, XmlBean>) this.getServletContext().getAttribute("struts");
		XmlBean xml = map.get(path);
		String formClass = xml.getFormClass();
		ActionForm form = FullForm.full(formClass, req);
		String actionType = xml.getActionClass();
		Action action = null;
		String url = "";
		try {
			Class clazz = Class.forName(actionType);
			action = (Action) clazz.newInstance();
			url = action.execute(req, form, xml.getActionForward());

		} catch (Exception e) {
			// TODO: handle exception
		}
		RequestDispatcher dis = req.getRequestDispatcher(url);
		dis.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.doGet(req, resp);
	}

	private String getPath(String servletPath) {
		return servletPath.split("\\.")[0];
	}

}
