package struts.form;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

public class StrutsXML {

	public StrutsXML() {
	}

	public static Map<String, XmlBean> ddd(String xmlPath) throws JDOMException, IOException {
		SAXBuilder builder = new SAXBuilder();
		Document document = builder.build(new File(xmlPath));
		Element root = document.getRootElement();// 根节点struts
		Map<String, XmlBean> rmap = new HashMap<String, XmlBean>();
		Element actionroot = root.getChild("action-mapping");
		List<Element> actio = actionroot.getChildren();// action节点
		for (Element e : actio) {
			XmlBean action = new XmlBean();
			String name = e.getAttributeValue("name");
			action.setBeanName(name);
			Element actionForm = root.getChild("formbeans");// formbeans节点
			List<Element> form = actionForm.getChildren();// bean节点
			for (Element ex : form) {
				if (name.equals(ex.getAttributeValue("name"))) {
					String formClass = ex.getAttributeValue("class");
					action.setFormClass(formClass);
					break;
				}
			}

			String path = e.getAttributeValue("path");
			action.setPath(path);
			String type = e.getAttributeValue("type");
			action.setActionClass(type);
			List<Element> forward = e.getChildren();
			Map<String, String> map = new HashMap<String, String>();
			for (Element x : forward) {
				String fname = x.getAttributeValue("name");
				String fvalue = x.getAttributeValue("value");
				// System.out.println("forward:name" + fname + "||value" +
				// fvalue);
				map.put(fname, fvalue);
			}
			// System.out.println("action:name" + name + "||path" + path +
			// "||type" + type);
			action.setActionForward(map);
			rmap.put(path, action);
		}
		return rmap;
	}

	public static void main(String[] args) throws JDOMException, IOException {
		Map<String, XmlBean> map = new StrutsXML().ddd("src/struts-config.xml");
		Set<String> set = map.keySet();
		for (Iterator<String> it = set.iterator(); it.hasNext();) {
			String key = it.next();
			System.out.println(key);
			System.out.println(map.get(key));
		}
	}
}
