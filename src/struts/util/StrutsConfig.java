package struts.util;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

public class StrutsConfig {

	public StrutsConfig() {
	}

	public void ddd() throws JDOMException, IOException {
		SAXBuilder builder = new SAXBuilder();
		Document document = builder.build(new File("bin/struts-config.xml"));
		Element root = document.getRootElement();
		Element actionform = root.getChild("formbeans");
		List<Element> form = actionform.getChildren();
		for (Element e : form) {
			String name = e.getAttributeValue("name");
			String clas = e.getAttributeValue("class");
			System.out.println("formbeans:name" + name + "||class" + clas);
		}
		Element actionroot = root.getChild("action-mapping");
		List<Element> actio = actionroot.getChildren();
		for (Element e : actio) {
			String name = e.getAttributeValue("name");
			String path = e.getAttributeValue("path");
			String type = e.getAttributeValue("type");
			List<Element> forward = e.getChildren();
			for (Element x : forward) {
				String fname = x.getAttributeValue("name");
				String fvalue = x.getAttributeValue("value");
				System.out.println("forward:name" + fname + "||value" + fvalue);
			}
			System.out.println("action:name" + name + "||path" + path + "||type" + type);
		}

	}

	public static void main(String[] args) throws JDOMException, IOException {
		new StrutsConfig().ddd();
	}
}
