package service.vo;

public class MessageVo {

	public MessageVo() {
	}

	private String mess = "";
	private String name = "";
	private String ssss = "";
	private String dddd = "";

	public String getMess() {
		return mess;
	}

	public void setMess(String mess) {
		this.mess = mess;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSsss() {
		return ssss;
	}

	public void setSsss(String ssss) {
		this.ssss = ssss;
	}

	public String getDddd() {
		return dddd;
	}

	public void setDddd(String dddd) {
		this.dddd = dddd;
	}

}
