package service.myService.panduan;

import java.util.HashMap;
import java.util.Map;

import service.vo.MessageVo;

public class PanduanServiceImpl implements PanduanService {

	public PanduanServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Map<String, MessageVo> getVo(String user) {
		boolean b = this.isUser(user);
		String url = "";
		MessageVo vo = new MessageVo();
		Map<String, MessageVo> map = new HashMap<String, MessageVo>();
		if (b) {
			vo.setDddd("dddd");
			vo.setMess("成功");
			vo.setName("name");
			vo.setSsss("ssss");
			map.put("/view/success.jsp", vo);
		} else {
			vo.setDddd("dddd");
			vo.setMess("失败");
			vo.setName("name");
			vo.setSsss("ssss");
			map.put("/view/fails.jsp", vo);
		}
		return map;
	}

	private boolean isUser(String user) {
		boolean b = false;
		if (user.equals("kim")) {
			b = true;
		}
		return b;
	}

}
