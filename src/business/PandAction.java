package business;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import service.myService.panduan.PanduanService;
import service.myService.panduan.PanduanServiceImpl;
import service.vo.MessageVo;
import struts.action.Action;
import struts.form.ActionForm;

public class PandAction implements Action {

	public PandAction() {
	}

	@Override
	public String execute(HttpServletRequest request, ActionForm form, Map actionForward) {
		String url = "";
		PanduanService service = new PanduanServiceImpl();
		Map<String, MessageVo> map = service.getVo(request.getParameter("text"));
		Set<String> set = map.keySet();
		for (Iterator<String> it = set.iterator(); it.hasNext();) {
			url = it.next();
			request.setAttribute("vo", map.get(url));
		}
		return url;
	}
}
