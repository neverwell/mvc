<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="ISO-8859-1"%>
<%@ page import="service.vo.MessageVo"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%
		MessageVo vo = (MessageVo) request.getAttribute("vo");
	%>
	<table border="2" width="85%">
		<tr>
			<td><%=vo.getDddd()%></td>
			<td><%=vo.getMess()%></td>
		</tr>
		<tr>
			<td><%=vo.getName()%></td>
			<td><%=vo.getSsss()%></td>
		</tr>
	</table>
</body>
</html>